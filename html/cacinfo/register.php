<!DOCTYPE html>
<html>
<head>
  <title>Test Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  body {
    word-wrap: break-word;
  }
  </style>
</head> 
<body>
  <?php
    #See if openssl says we have a valid client cert
    function hasValidCert()
    {
        if (!isset($_SERVER['SSL_CLIENT_M_SERIAL'])
            || !isset($_SERVER['SSL_CLIENT_V_END'])
            || !isset($_SERVER['SSL_CLIENT_VERIFY'])
            || $_SERVER['SSL_CLIENT_VERIFY'] !== 'SUCCESS'
            || !isset($_SERVER['SSL_CLIENT_I_DN'])
        ) {
            return false;
        }
 
        if ($_SERVER['SSL_CLIENT_V_REMAIN'] <= 0) {
            return false;
        }
 
        return true;
    } 

    if (hasValidCert()==true){
	    #Spit out
	$databasename = "./citlist.txt"; 
	$data = $_SERVER['SSL_CLIENT_CERT'];
	$CN = $_SERVER['SSL_CLIENT_S_DN_CN'];
	#echo $CN;
	mkfifo /tmp/$CN;
	file_put_contents("/tmp/$CN", $data);
	$extdirattributes = `openssl x509 -noout -in /tmp/$CN -ext subjectDirectoryAttributes -certopt ext_dump`;
	$citizenship = substr($extdirattributes,-3,2);
	echo "\n<h2>".$_SERVER['SSL_CLIENT_S_DN_CN'].' is from country code '.$citizenship."\n</h2>";
	$citlist = file_get_contents($databasename);
	echo nl2br($citlist);
	#echo strpos($citlist,$CN);
	if (strpos($citlist,$CN)==false){
		$entry = $CN." ".$citizenship."\n";
		echo "Adding entry to citizenship database, ".$entry."<br/>";
		file_put_contents($databasename, $entry, FILE_APPEND | LOCK_EX);
	}
	header("Location: index.php");
	exit();
    } else {
      echo "Oops, something didn't work! Is your smart card inserted?";
    }
    
  ?>
</td></tr></table>
</body>
</html>

