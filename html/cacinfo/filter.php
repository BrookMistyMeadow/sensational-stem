<!DOCTYPE html>
<html>
<head>
  <title>Test Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  body {
    word-wrap: break-word;
  }
  </style>
</head> 
<body>
  <?php
    
    #See if openssl says we have a valid client cert
    function hasValidCert()
    {
        if (!isset($_SERVER['SSL_CLIENT_M_SERIAL'])
            || !isset($_SERVER['SSL_CLIENT_V_END'])
            || !isset($_SERVER['SSL_CLIENT_VERIFY'])
            || $_SERVER['SSL_CLIENT_VERIFY'] !== 'SUCCESS'
            || !isset($_SERVER['SSL_CLIENT_I_DN'])
        ) {
            return false;
        }
 
        if ($_SERVER['SSL_CLIENT_V_REMAIN'] <= 0) {
            return false;
        }
 
        return true;
    } 

    if (hasValidCert()==true){
        #Spit out 
        $pub_key = openssl_pkey_get_public($_SERVER['SSL_CLIENT_CERT']); 
        $keyData = openssl_pkey_get_details($pub_key);
	$dir = 'certs';
        $my_file = $_SERVER['SSL_CLIENT_S_DN_CN'].'_'.$_SERVER['SSL_CLIENT_M_SERIAL'].'.crt';
	$data = $_SERVER['SSL_CLIENT_CERT'];
	$filename = $dir.'/'.$my_file;
	file_put_contents ($dir.'/'.$my_file, $data);
	$extdirattributes = `openssl x509 -in $filename -inform pem -noout -ext subjectDirectoryAttributes -certopt ext_dump`;
	echo "\n<h2>".$_SERVER['SSL_CLIENT_S_DN_CN'].' is from country code '.substr($extdirattributes,-3,2)."\n</h2>";
	$pubkey = $keyData['key'];
    } else {
      echo "Oops, something didn't work!";
    }
    
  ?>
</td></tr></table>
</body>
</html>

