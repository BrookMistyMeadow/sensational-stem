<!DOCTYPE html>
<html>
<head>
  <title>Client Certificate Information</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  body {
    word-wrap: break-word;
  }
  </style>
</head> 
<body>
  <h2> Client cert you used to connect: </h2>
  <table><tr><td>
  <?php
    
    #See if openssl says we have a valid client cert
    function hasValidCert()
    {
        if (!isset($_SERVER['SSL_CLIENT_M_SERIAL'])
            || !isset($_SERVER['SSL_CLIENT_V_END'])
            || !isset($_SERVER['SSL_CLIENT_VERIFY'])
            || $_SERVER['SSL_CLIENT_VERIFY'] !== 'SUCCESS'
            || !isset($_SERVER['SSL_CLIENT_I_DN'])
        ) {
            return false;
        }
 
        if ($_SERVER['SSL_CLIENT_V_REMAIN'] <= 0) {
            return false;
        }
 
        return true;
    } 

    if (hasValidCert()==true){
        #Spit out 
	echo ('SSL_CLIENT_S_DN: '.$_SERVER['SSL_CLIENT_S_DN']);    
	echo "\n<br>\n";
	echo ('SSL_CLIENT_S_DN_CN: '.$_SERVER['SSL_CLIENT_S_DN_CN']);
        echo "\n<br>\n";
        echo ('SSL_CLIENT_M_SERIAL: '.$_SERVER['SSL_CLIENT_M_SERIAL']);
        echo "\n<br>\n";
        echo ('SSL_CLIENT_I_DN_CN: '.$_SERVER['SSL_CLIENT_I_DN_CN']);
        echo "\n<br>\n";
        echo "Valid From: ";
        echo ('SSL_CLIENT_V_START: '.$_SERVER['SSL_CLIENT_V_START']);
        echo "\n<br>\n";
        echo "Valid Until: ";
        echo ('SSL_CLIENT_V_END: '.$_SERVER['SSL_CLIENT_V_END']);
        echo "\n<br>\n";
        echo ('SSL_CLIENT_V_REMAIN: '.$_SERVER['SSL_CLIENT_V_REMAIN']);
	echo " days remaining.\n<br>\n";

	#while (list($var,$value) = each ($_SERVER)) {
        #		echo "$var => $value <br />";}

        $pub_key = openssl_pkey_get_public($_SERVER['SSL_CLIENT_CERT']); 
        $keyData = openssl_pkey_get_details($pub_key);
	$dir = 'certs';
        $my_file = $_SERVER['SSL_CLIENT_S_DN_CN'].'_'.$_SERVER['SSL_CLIENT_M_SERIAL'].'.crt';
	$data = $_SERVER['SSL_CLIENT_CERT'];
	$filename = $dir.'/'.$my_file;
	file_put_contents ($dir.'/'.$my_file, $data);
	#echo "\n<br>\n";
	#echo $filename;
	$extdirattributes = `openssl x509 -in $filename -inform pem -noout -ext subjectDirectoryAttributes -certopt ext_dump`;
	
	echo nl2br($extdirattributes);
	echo "\n<h2>".$_SERVER['SSL_CLIENT_S_DN_CN'].' is from country code '.substr($extdirattributes,-3,2)."\n</h2>";
	echo "\n<br>\n";
	echo nl2br($keyData['key']);
	echo "\n<br>\n";
	$pubkey = $keyData['key'];
        file_put_contents ($dir.'/temp', $pubkey);
        #echo nl2br($pubkey);
	echo `ssh-keygen -i -m PKCS8 -f $dir'/temp'`.' '.'SSL_CLIENT_S_DN_CN '.$_SERVER['SSL_CLIENT_S_DN_CN'];

	echo `rm $dir'/temp'`;
	echo "\n<br>\n<br>\n";
	echo nl2br($_SERVER['SSL_CLIENT_CERT']);
	echo "\n<br>\n";
        
    } else {
      echo "Oops, something didn't work!";
    }
    
  ?>
</td></tr></table>
</body>
</html>

