
      hideCode();
      var editor = ace.edit("editor",{
         mode: "ace/mode/html",
         selectionStyle: "text" });
         editor.setTheme("ace/theme/monokai");
     editor.setOptions({
         autoScrollEditorIntoView: true,
         copyWithEmptySelection: true,
     });

      
     function LoadMe(){
         editor.setValue(loadFile("./index.html"));
     }
                
		function SaveMe(){

			var text = editor.getValue();
			var data = new FormData();
			data.append("data" , text);
			var xhr = new XMLHttpRequest();
			xhr.open( 'post', 'https://harris.home.dyndns.org/bryan/quill/savefile.php', true );
			xhr.send(data);

		}

		function loadFile(filePath) {
  			var result = null;
  			var xmlhttp = new XMLHttpRequest();
 	 		xmlhttp.open("GET", filePath, false);
  			xmlhttp.send();
  			if (xmlhttp.status==200) {
    				result = xmlhttp.responseText;
  			}
  			return result;
		}
                function showCode(){
                   $("#source").show();
                   $(".showbutton").hide();
                   
                }
                function hideCode(){
                   $("#source").hide();
                   $(".showbutton").show();
                }
		$(document).ready(function(){
                     LoadMe(); 
		});
				
