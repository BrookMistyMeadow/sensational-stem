function myFunction(element) {
 element.classList.toggle("myactive");
  var x = element.nextElementSibling;
  if (x.style.display === "block") { x.style.display = "none"; } else {x.style.display = "block";}
  }

$(document).ready(
function(){
    $(".field--entity-reference-target-type-taxonomy-term").removeClass("field--entity-reference-target-type-taxonomy-term");
    $("#edit-field-register-0-target-id").hide();
    $(".js-form-item-field-register-0-settings-default-data").hide();
    $("#edit-field-register-0-settings-token-tree-link").hide();
    $("#edit-field-request-0-settings-token-tree-link").hide();
    $(".js-form-item-field-register-0-settings-webform-access-group").hide();
    $(".js-form-item-field-request-0-settings-webform-access-group").hide();
    $(".js-form-item-field-end-0-value-time").hide();
    $(".js-form-item-field-start-0-value-time").hide();
    $(".js-form-item-field-request-0-settings-default-data").hide();
    $(".js-form-item-field-request-0-target-id").hide();
    $(".field--name-name").hide();
    $("#password-policy-status").hide();
    $(".field--name-field-lonf-title").wrapInner("<h5></h5>");
    $(".statistics-counter").css("display","none");

});
