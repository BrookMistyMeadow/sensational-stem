<!DOCTYPE html>
<html>
<head>
  <title>Test Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
  body {
    word-wrap: break-word;
  }
  </style>
</head> 
<body>
  <h2> Client cert you used to connect: </h2>
  <table><tr><td>
  <?php
    
    #See if openssl says we have a valid client cert
    function hasValidCert()
    {
        if (!isset($_SERVER['SSL_CLIENT_M_SERIAL'])
            || !isset($_SERVER['SSL_CLIENT_V_END'])
            || !isset($_SERVER['SSL_CLIENT_VERIFY'])
            || $_SERVER['SSL_CLIENT_VERIFY'] !== 'SUCCESS'
            || !isset($_SERVER['SSL_CLIENT_I_DN'])
        ) {
            return false;
        }
 
        if ($_SERVER['SSL_CLIENT_V_REMAIN'] <= 0) {
            return false;
        }
 
        return true;
    } 

    if (hasValidCert()==true){
        #Spit out 
        echo ('SSL_CLIENT_S_DN_CN '.$_SERVER['SSL_CLIENT_S_DN_CN']);
        echo "\n<br>\n";
        echo ('SSL_CLIENT_M_SERIAL '.$_SERVER['SSL_CLIENT_M_SERIAL']);
        echo "\n<br>\n";
        echo ('SSL_CLIENT_I_DN_CN '.$_SERVER['SSL_CLIENT_I_DN_CN']);
        echo "\n<br>\n";
        echo "Valid From: ";
        echo ($_SERVER['SSL_CLIENT_V_START']);
        echo "\n<br>\n";
        echo "Valid Until: ";
        echo ($_SERVER['SSL_CLIENT_V_END']);
        echo "\n<br>\n";
        echo ($_SERVER['SSL_CLIENT_V_REMAIN']);
        echo " days remaining.";

        echo "\n<br>\n<br>\n";
        $pub_key = openssl_pkey_get_public($_SERVER['SSL_CLIENT_CERT']); 
        $keyData = openssl_pkey_get_details($pub_key);
        
        echo nl2br($keyData['key']);
        echo "\n<br>\n";
        
        $pubkey = $keyData['key'];
        echo `ssh-keygen -i -m PKCS8 -f /dev/stdin <<< "$pubkey"`;
        echo " ";
        echo ('SSL_CLIENT_S_DN_CN '.$_SERVER['SSL_CLIENT_S_DN_CN']);
        
        echo "\n<br>\n<br>\n";
        #echo $_SERVER['SSL_CLIENT_S_DN_CN'].'_'.$_SERVER['SSL_CLIENT_M_SERIAL'].'.crt';
        #echo "\n<br>\n";
        echo nl2br($_SERVER['SSL_CLIENT_CERT']);
        
        $dir = 'certs';
        $my_file = $_SERVER['SSL_CLIENT_S_DN_CN'].'_'.$_SERVER['SSL_CLIENT_M_SERIAL'].'.crt';
        $data = $_SERVER['SSL_CLIENT_CERT'];
        file_put_contents ($dir.'/'.$my_file, $data);
        
    } else {
      echo "Oops, something didn't work!";
    }
    
  ?>
</td></tr></table>
</body>
</html>

